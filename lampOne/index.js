var mqtt_client = require("mqtt");
var bodyParser = require('body-parser');
var cors = require("cors");
var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
app.use(cors());
app.use(bodyParser.json());

var topic='';                 // per serve perchĂ¨ creo il topic dal body del messaggio

var auth = {                // credenziali per l'accesso
    username:"fritz",
    password:"logida1"
};

var client  = mqtt_client.connect('mqtt://51.145.240.20',auth);       // connessione

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

client.on('connect', function () {          // connessione e subscribe al topic
    console.log("connect");
    client.subscribe('/+/+/read');
});
client.on('message',(topic,data)=>{
    io.emit('kitchen',data);
    console.log("EMIT DATA");
});
app.post('/Kitchen', function(req, res){            // ricezione del messaggio e publish nel topic
    console.log(req.body);

    var body = JSON.parse(JSON.stringify(req.body));

    topic = "/"+body.scheda_id+'/0'+body.slot+'/cmd';

    var msg = {
        "command":body.status
    };

    console.log('topic  ' + topic);
    console.log('msg    ' + JSON.stringify(msg));

    client.publish(topic, JSON.stringify(msg));
    res.status(200).json({success: true});
});

client.on('message', function (topic, message) {            // ricezione dei messaggi
    var str = message.toString();

    //console.log(str);
});

const port = process.env.PORT || 1337;

http.listen(port, () => {
    console.log("Server running on port " + port);
});

function create_topic(body){

}