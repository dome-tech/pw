﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAppDemov2.Models;
using WebAppDemov2.Sensors;

namespace WebAppDemov2.Data
{
    public interface IDataAccess
    {
        // inserimento nuovo utente -> registrazione
        int insert_user(string nome, string cognome, string codice_fiscale, string email, string telefono, DateTime data_nascita, string username, string password);

        // get delle credenziali di login di un utente -> login
        LoginClass get_login(string new_username, string new_password);

        // get delle schede e device in quella posizione
        IEnumerable<Device> get_device(string new_posizione);
    }
}
