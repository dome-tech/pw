﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using WebAppDemov2.Models;
using Microsoft.Extensions.Configuration;
using MySql.Data;
using MySql.Data.MySqlClient;
using WebAppDemov2.Sensors;

namespace WebAppDemov2.Data
{
    public class DataAccess : IDataAccess
    {
        private string _connectionString;
        public DataAccess(IConfiguration config)
        {
            this._connectionString = config.GetConnectionString("dometech");
        }

        public int insert_user(string nome, string cognome, string codice_fiscale, string email, string telefono, DateTime data_nascita, string username, string password)
        {
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();
                var user = new RegistrationClass(nome, cognome, codice_fiscale, email, telefono, data_nascita, username, password);

                var id = connection.ExecuteScalar<int>(query_insert, user);

                connection.Close();

                return id;
            }
        }

        public LoginClass get_login(string new_username, string new_password)
        {
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                var user = connection.QueryFirstOrDefault<LoginClass>(query_select_by_up, new { username = new_username,  password = new_password });

                connection.Close();

                return user;
            }
        }

        public IEnumerable<Device> get_device(string new_posizione)
        {
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();
                var device = connection.Query<Device>(query_select_device, new { posizione = new_posizione }).ToList();
                connection.Close();

                return device;
            }
        }
        const string query_insert = @"
INSERT INTO users
    (
        nome,
        cognome,
        codice_fiscale,
        email,
        telefono,
        data_nascita,
        username,
        password
    )
VALUES
    (
        @Nome,
        @Cognome,
        @CodiceFiscale,
        @Email,
        @Telefono,
        @DataNascita,
        @Username,
        @Password
    )";

        const string query_select_by_up = @"
SELECT 
    id,
    nome,
    cognome,
    username,
    password
FROM 
    users
WHERE 
    username=@username and
    password=@password";

        const string get = @"
select * from scheda, device";

        const string query_select_device = @"
SELECT 
    s.id as IdScheda,
    s.nome as NomeScheda,
    s.indirizzo as IndirizzoScheda,
    s.posizione as PosizioneScheda,
    s.descrizione as DescrizioneScheda,
    d.id,
    d.nome,
    d.slot,
    d.descrizione,
    d.tipo,
    d.min_value,
    d.max_value
FROM 
    scheda s
INNER JOIN 
    device d
    on s.id = d.scheda_id
WHERE 
    s.posizione=@posizione";
    }
}
