﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAppDemov2.Data;
using WebAppDemov2.Models;

namespace WebAppDemov2.Pages.LogReg
{
    [IgnoreAntiforgeryToken]
    public class LoginModel : PageModel
    {
        [BindProperty]
        public LoginClass Login { get; set; }

        private IDataAccess _data;

        public LoginModel(IDataAccess data)
        {
            this._data = data;
        }

        public void OnGet()
        {

        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                LoginClass login = this._data.get_login(Login.Username, Login.Password);

                if (login.Equals(null))
                {
                    AttributeClass.set_log(false);
                    // ERRORE -> FAR APPARIRE CON SIGNALAR MESSAGGIO DI ERRORE 
                }
                else
                {
                    AttributeClass.set_log(true);
                    return RedirectToPage("/Test/HomeDetails");        // REDIRECT PAGINA CON CASA
                }
            }

            return Page();
        }
    }
}