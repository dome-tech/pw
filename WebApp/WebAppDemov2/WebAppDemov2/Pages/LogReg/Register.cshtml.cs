﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAppDemov2.Data;
using WebAppDemov2.Models;

namespace WebAppDemov2.Pages.Login
{
    [IgnoreAntiforgeryToken]
    public class RegisterModel : PageModel
    {
        [BindProperty]
        public RegistrationClass User { get; set; }

        private IDataAccess _data;

        public RegisterModel(IDataAccess data)
        {
            this._data = data;
        }

        public void OnGet()
        {

        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var id=0;

                if (User.Password.CompareTo(User.ConfermaPwd)==0)
                    id = this._data.insert_user(User.Nome, User.Cognome, User.CodiceFiscale, User.Email, User.Telefono, User.DataNascita, User.Username, User.Password);
                else
                {
                    id = 0;
                }

                if (!id.Equals(""))
                    return RedirectToPage("/Index");
            }

            return Page();
        }
    }
}