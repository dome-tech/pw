﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebAppDemov2.Data;
//using WebAppDemov2.MqttSender;
using WebAppDemov2.Sensors;
using WebAppDemov2.SocketIO;
using WebSocketSharp;
using System.Net;
using System.IO;

namespace WebAppDemov2.Pages.Test
{

    [IgnoreAntiforgeryToken]
    public class KitchenModel : PageModel
    {
      /*  [BindProperty]
        public Led Input { get; set; }*/
        public Led leds { get; set; }
        public Temperatura temp { get; set; }
        public IEnumerable<Device> device { get; set; }

        //private IMessageService _sender;
        private IDataAccess _data;

        //public WebRequest wb { get; set; }

        // Usa il costruttore con il singleton
        public KitchenModel(/*IMessageService sender,*/ IDataAccess data)
        {
            //this._sender = sender;
            this._data = data;
        }

        public void OnGet()
        {
            device = this._data.get_device("Cucina");

            foreach (var a in device)
            {
                if (!a.Equals(null))
                {
                    if (a.Nome.ToUpper().Contains("LED"))
                        leds = new Led(a, "ON");
                    else if (a.Nome.ToUpper().Contains("TEMPERATURA"))
                        temp = new Temperatura(a, "23°C");
                }
            }
        }

        public IActionResult OnPost()
        {
            //Led[] leds;
            

            /*if (leds.Count > 0)
            {
                foreach (var l in leds)
                {
                    _sender.sendMessage((Led)l);
                }
            }*/
            

            /*if (login.Equals(null))
            {
                AttributeClass.set_log(false);
                // ERRORE -> FAR APPARIRE CON SIGNALAR MESSAGGIO DI ERRORE 
            }
            else
            {
                AttributeClass.set_log(true);
                return RedirectToPage("/Test/HomeDetails");        // REDIRECT PAGINA CON CASA
            }
            _sender.sendMessage(Input);*/

            return Page();
        }
    }
}