﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebAppDemov2.Data;

namespace WebAppDemov2.Models
{
    public class LoginClass
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "Invalid Input")]
        public string Password { get; set; }

        public LoginClass()
        {
            this.Username = "";
            this.Password = "";
        }

        public LoginClass(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }
    }
}
