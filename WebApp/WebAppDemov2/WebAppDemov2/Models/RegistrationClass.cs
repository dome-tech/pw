﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppDemov2.Models
{
    public class RegistrationClass
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public string Cognome { get; set; }

        [Required]
        [StringLength(16, ErrorMessage = "Codice fiscale non valido")]
        public string CodiceFiscale { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [StringLength(10)]
        public string Telefono { get; set; }

        public DateTime DataNascita { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "Invalid Input")]
        public string Password { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "Invalid Input")]
        public string ConfermaPwd { get; set; }

        public RegistrationClass()
        {
            this.Nome = "Pippo";
            this.Cognome = "Baudo";
            this.CodiceFiscale = "PPPBBB12P63B156P";
            this.Email = "pippo@baudo.it";
            this.Telefono = "1234567891";
            this.DataNascita = DateTime.Now;
            this.Password = "pippobaudo";
            this.Username = "pippobaudo";
        }

        public RegistrationClass(string nome, string cognome, string codice_fiscale, string email, string telefono, DateTime data_nascita, string username, string password)
        {
            this.Nome = nome;
            this.Cognome = cognome;
            this.CodiceFiscale = codice_fiscale;
            this.Email = email;
            this.DataNascita = data_nascita;
            this.Telefono = telefono;
            this.Password = password;
            this.Username = username;
        }
    }
}
