﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppDemov2.Sensors
{
    public class Led:Device
    {
        public string _command { get; set; }

        public Led(string cmd)
        {
            this._command = cmd;
        }

        public Led(Device device, string cmd)
        {
            this.Id = device.Id;
            this.Nome = device.Nome;
            this.Slot = device.Slot;
            this.Descrizione = device.Descrizione;
            this.Tipo = device.Tipo;
            this.MaxValue = device.MaxValue;
            this.MinValue = device.MinValue;
            this.IdScheda = device.IdScheda;
            this.Command = cmd;
            this._command = cmd;
        }
    }
}
