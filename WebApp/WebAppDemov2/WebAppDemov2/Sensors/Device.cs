﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppDemov2.Sensors
{
    public class Device
    {
        // DETTAGLI DEVICE
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Descrizione { get; set; }

        public string Slot { get; set; }

        public string Tipo { get; set; }

        public decimal MaxValue { get; set; }

        public decimal MinValue { get; set; }


        // DETTAGLI SCHEDA
        public int IdScheda { get; set; }

        public string NomeScheda { get; set; }

        public string PosizioneScheda { get; set; }

        public string IndirizzoScheda { get; set; }

        public string DescrizioneScheda { get; set; }

        // ALTRO
        public string Command { get; set; }
        public string Value { get; set; }

        public Device(int id, string nome, string descrizione, string slot, string tipo, decimal max_value, decimal min_value, int id_scheda, string nome_scheda, string posizione_scheda, string indirizzo_scheda, string descrizione_scheda)
        {
            this.Id = id;
            this.Nome = nome;
            this.Descrizione = descrizione;
            this.Slot = slot;
            this.Tipo = tipo;
            this.MaxValue = max_value;
            this.MinValue = min_value;
            this.IdScheda = id_scheda;
            this.NomeScheda = nome_scheda;
            this.PosizioneScheda = posizione_scheda;
            this.IndirizzoScheda = indirizzo_scheda;
            this.DescrizioneScheda = descrizione_scheda;
            this.Command = "OFF";
            this.Value = "23°C";
        }

       /* public Device(Device a)
        {
            this.Id = a.id;
            this.Nome = a.nome;
            this.Descrizione = a.descrizione;
            this.Slot = a.slot;
            this.Tipo = a.tipo;
            this.MaxValue = a.max_value;
            this.MinValue = a.min_value;
            this.IdScheda = a.id_scheda;
            this.NomeScheda = a.nome_scheda;
            this.PosizioneScheda = a.posizione_scheda;
            this.IndirizzoScheda = a.indirizzo_scheda;
            this.DescrizioneScheda = a.descrizione_scheda;
        }*/

        public Device() {
            this.Command = "OFF";
            this.Value = "45.5 °C";
        }
    }
}
