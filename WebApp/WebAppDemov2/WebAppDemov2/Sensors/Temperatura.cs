﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppDemov2.Sensors
{
    public class Temperatura: Device
    {
        public string _value { get; set; }

        public Temperatura(string val)
        {
            this._value = val;
        }

        public Temperatura(Device device, string val)
        {
            this.Id = device.Id;
            this.Nome = device.Nome;
            this.Slot = device.Slot;
            this.Descrizione = device.Descrizione;
            this.Tipo = device.Tipo;
            this.MaxValue = device.MaxValue;
            this.MinValue = device.MinValue;
            this.IdScheda = device.IdScheda;
            this.Value = val;
            this._value = val;
        }
    }
}
