const SerialPort = require('serialport')
const port = new SerialPort('/dev/ttyUSB0', function (err) {
  if (err) {
    return console.log('Error: ', err.message)
  }
})

port.write('1AC', function(err) {
  if (err) {
    return console.log('Error on write: ', err.message)
  }
  console.log('message written')
})

