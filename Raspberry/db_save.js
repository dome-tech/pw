let config = {
    "host": "localhost",
    "user": "root",
    "password":"",
    "database":"oee_exc"
};
const mysql = require("mysql");
var con = mysql.createConnection(config);
const { Client } = require('pg')
const client = new Client({
  user: 'admin',
  host: 'localhost',
  database: 'dometech',
  password: 'logida1',
});

client.connect();


var Influx = require('influx');
exports.getDevices = function(dv,bd){
    return new Promise(function(resolve,reject){

        con.connect(function(err) {
            let query = "SELECT * FROM device WHERE slot = $1 AND scheda_id = $2";
            client.query(query,[dv,bd], (err, res) => {
                //      console.log(err ? err.stack : res) // Hello World!
                resolve(res.rows[0]) ;
            })
        });

    });
};
exports.getBoards = function(){
    con.connect(function(err) {
        let query = "QUERY PER LE BOARD(O ROBE COSI)";
        con.query(query,(err,result)=>{
            if(err){
                console.log(err);
                res.status(500).json({message:"ERROR"});
            }
            else{
                res.status(200).json({success:true,status:result});
            }
        })
    });
};
const influx = new Influx.InfluxDB({
    host: 'localhost',
    database: 'DomeTech',
    schema: [
        {
            measurement: 'data',
            fields: {
                value:Influx.FieldType.INTEGER
            },
            tags: [
                'board',
                'device'
            ]
        }
    ]
});
exports.saveInflux=function(board,device,data){
    influx.writePoints([
        {
            measurement: 'data',
            tags: { 'board':board,'device':device },
            fields: { value:data}
        }
    ]);
}
