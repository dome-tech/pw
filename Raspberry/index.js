var mqtt = require('./mqtt_client.js');
const SerialPort = require('serialport');
const port = new SerialPort('/dev/ttyUSB0', function (err) {
    if (err) {
        return console.log('Error: ', err.message)
    }
});
var db = require('./db_save');
var boardP = /[0-9+]/;
var boardD = /[0-9]+[0-9]+/;
var discTimeout;
var nextTimeout;
port.on('data',function(data){
    clearTimeout(discTimeout);
    var rx = data.toString('utf8').substring(0,4);
    var value = data.slice(4);
    //  var value = parseInt(rx.charAt(4));
	console.log(rx);
    var simMsg = {
        "value":parseInt(value.readUIntBE(0,4).toString()),
        "device_id":rx.charAt(3),
        "board_id":rx.charAt(1)
    };
    if(rx.charAt(1)==lastBoard.address){
        elaborate_data(simMsg.board_id,simMsg.device_id,simMsg.value).then(function(data){
            simMsg.value = data;

            messagesToSend.push(simMsg);
            discTimeout = setTimeout(function(){
                clearTimeout(nextTimeout);
                lastBoard.connected = 0;
                console.log(lastBoard);
                SendCommands();
                pollNextBoard();
            },1000);
            nextTimeout = setTimeout(function(){
                SendCommands();
                pollNextBoard();
            },200);

            console.log("NEXT");
        })

    }

    //db.saveInflux(rx.charAt(1),3,parseInt(value.readUIntBE(0,4).toString()));
    
});
var db = require('./db_save');
var boards = [
    {
        "id":1,
        "address":'1',
        "devices":[
            {
                "id":1,
                "name":"Lampadina 1",
                "type":"led"
            }
        ]
    },
    {
        "id":2,
        "address":'2',
        "devices":[
            {
                "id":1,
                "name":"Lampadina 1",
                "type":"led"
            }
        ]
    },
 /*   {
        "id":3,
        "address":'3',
        "devices":[
            {
                "id":1,
                "name":"Lampadina 1",
                "type":"led"
            }
        ]
    }*/
];
var simulatorCounter = 0;
var boardMsgCounter = 0;
var messagesToSend = [];
var commandsToSend = [];
var sendCommand = 0;
var hasRxLastBoard = 1;
var lastBoard;
pollNextBoard();

setInterval(function(){
   if(mqtt.connected === true&&messagesToSend.length>0){
       var msg = messagesToSend.shift();
       mqtt.sendMsg('/'+msg.board_id+'/'+msg.device_id+'/read',JSON.stringify(msg));
   }
},10);
function pollNextBoard(){

    if(!sendCommand){
        /*send message to boards[i]*/
        //boardCOM.send(boards[boardMsgCounter].address,boards[boardMsgCounter].devices[0].id,"ON")
        port.write(boards[boardMsgCounter].address+'MC00000', function(err) {
            if (err) {
                return console.log('Error on write: ', err.message)
            }

        });
        console.log("Call command:"+boards[boardMsgCounter].address+'MC00000');
        lastBoard = boards[boardMsgCounter];
        if(boardMsgCounter === (boards.length-1)){
            boardMsgCounter=0;
        }else{
            boardMsgCounter++;
        }
    }
}
/*SIMULATOR*/
/*setInterval(function(){
    var simMsg = {
        "temperature":Math.floor(Math.random() * 10),
        "humidity":Math.floor(Math.random() * 10),
        "light":Math.floor(Math.random() * 10),
        "board_id":simulatorCounter
    };
    messagesToSend.push(simMsg);
    if(simulatorCounter === 5)simulatorCounter = 1;
    else simulatorCounter++;
},2000);*/

mqtt.client.on('message',function(topic,message){
   console.log("message received");
   var  board = boardP.exec(topic.toString());
   var device = boardD.exec(topic.toString());
   var msg = JSON.parse(message.toString());
   var cmd = '0';
   if(msg.command == "ON")cmd = 'A';
   else cmd = 'B';
   console.log(board+'M'+cmd+device.toString().charAt(1)+'0000');
   /*
    port.write(board+'M'+cmd+device.toString().charAt(1)+'0000', function(err) {
        if (err) {
            return console.log('Error on write: ', err.message)
        }
        console.log('message written')
    });*/
   commandsToSend.push(board+'M'+cmd+device.toString().charAt(1)+'0000');
});
function elaborate_data(board,device,value){
    return new Promise(function(resolve,reject){
        var dv;
        getDeviceInfo(board,device).then(function(data){
            dv = JSON.parse(JSON.stringify(data));
            var tValue = (((parseInt(dv.max_value) - parseInt(dv.min_value))/dv.split)*value)+parseInt(dv.min_value);
            if(board == 1)
                console.log(value);
            resolve (tValue);
        })
    })


}
function getDeviceInfo(board,device){
    return new Promise(function(resolve,reject){
        db.getDevices(device,board).then((function (data) {
            console.log(data);
            resolve(data);
        }));
    });

}
function SendCommands(){
    while(commandsToSend.length>0){
        port.write(commandsToSend.shift(), function(err) {
            if (err) {
                return console.log('Error on write: ', err.message)
            }
            console.log('message written')
        });
    }
}
