const SerialPort = require('serialport')
const port = new SerialPort('/dev/ttyUSB0', function (err) {
    if (err) {
        return console.log('Error: ', err.message)
    }
})
exports.send=function(frame){
    port.write(frame, function(err) {
        if (err) {
            return console.log('Error on write: ', err.message)
        }
        console.log('message written')
    });
};
exports.port = port;


