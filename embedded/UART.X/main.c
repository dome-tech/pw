/*
 * File:   main.c
 * Author: Utente
 *
 * Created on 8 maggio 2019, 15.56
 */

#pragma config FPLLIDIV = DIV_2 // PLL Input Divider (1x Divider)
#pragma config FPLLMUL = MUL_20 // PLL Multiplier (24x Multiplier)
#pragma config UPLLIDIV = DIV_2 // USB PLL Input Divider (12x Divider)
#pragma config UPLLEN = OFF // USB PLL Enable (Disabled and Bypassed)
#pragma config FPLLODIV = DIV_1 // System PLL Output Clock Divider (PLL Divide by 256)
// DEVCFG1
#pragma config FNOSC = PRIPLL // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = ON // Secondary Oscillator Enable (Enabled)
#pragma config IESO = ON // Internal/External Switch Over (Enabled)
#pragma config POSCMOD = HS // Primary Oscillator Configuration (HS osc mode)
#pragma config OSCIOFNC = ON // CLKO Output Signal Active on the OSCO Pin (Enabled)
#pragma config FPBDIV = DIV_8 // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/8)
#pragma config FCKSM = CSDCMD // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1048576 // Watchdog Timer Postscaler (1:1048576)
#pragma config FWDTEN = OFF // Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))
// DEVCFG0
#pragma config DEBUG = OFF // Background Debugger Enable (Debugger is disabled)
#pragma config ICESEL = ICS_PGx2 // ICE/ICD Comm Channel Select (ICE EMUC2/EMUD2 pins shared with PGC2/PGD2)
#pragma config PWP = OFF // Program Flash Write Protect (Disable)
#pragma config BWP = OFF // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF // Code Protect (Protection Disabled)
#define CONFIG1 (ADC_MODULE_ON | ADC_FORMAT_INTG32 | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON)
#define CONFIG2 (ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_ON |ADC_SAMPLES_PER_INT_2 | ADC_ALT_BUF_ON | ADC_ALT_INPUT_OFF)
#define CONFIG3 (ADC_CONV_CLK_INTERNAL_RC | ADC_SAMPLE_TIME_15)
#define CONFIGPORT (ENABLE_AN1_ANA | ENABLE_AN2_ANA)
#define CONFIGSCAN (SKIP_SCAN_AN0 | SKIP_SCAN_AN3 | SKIP_SCAN_AN4 |SKIP_SCAN_AN5 | SKIP_SCAN_AN6 | SKIP_SCAN_AN7 | SKIP_SCAN_AN8 | SKIP_SCAN_AN9 |SKIP_SCAN_AN10 | SKIP_SCAN_AN11 | SKIP_SCAN_AN12 | SKIP_SCAN_AN13 | SKIP_SCAN_AN14 |SKIP_SCAN_AN15)
// nella slide le righe vanno a capo, nel codice ogni #define va su un?unica riga

#include <p32xxxx.h>
#include <plib.h> // Include the PIC32 Peripheral Library.
#define BAUD_VALUE 9600
#define SYSCLK 80000000
#define ADDRESS '2'

char txData[4];
int i = 0;
char is_receiving = 0;
int old_btn = 0;
char old_btn2;
char data[4];
int flag = 0;
char device;
char rxDATA;
char ptm;
int send = 0;
char cmd = 0;

char devices[4] = {'0'};
int dvCounter = 0;

char addressTx = 0;
char addressRx = 0;
int counter = 0;
int number = 5;
char buzzerFlag = 0;
int measurement;
char result[1];
void delay(int t) {
    int n = t * 1900; //1900 � un numero ricavato sperimentalmente
    while (n > 0) {
        n--;
    }
}

void initializeADC(){
 CloseADC10(); // Generally, you should disable the ADC before setup.
 // Use ground as negative reference for channel A instead of pin AN1 (RB1)
 //SetChanADC10( ADC_CH0_NEG_SAMPLEA_NVREF);
 OpenADC10( CONFIG1, CONFIG2, CONFIG3, CONFIGPORT, CONFIGSCAN);
 // Setup for the ADC10.
 EnableADC10(); // Enables the ADC10.
}
void sendData(){
    char txHd[4]={'M',ADDRESS,'R',devices[dvCounter]};
    PORTDbits.RD8 = 1;
    
    while(BusyUART1());
    int j = 0;
    for(j=0;j<4;j++){
        putcUART1(txHd[j]);
    }
    for(j=0;j<4;j++){
        putcUART1(txData[j]);
    }
    while(BusyUART1());
    PORTDbits.RD8 = 0;
    if(dvCounter<0)dvCounter ++;
    else 
        dvCounter = 0;
                    
    send = 0;
}
void getAnalogMeasurement(char device)
{
     measurement = ReadADC10(0); // l?argomento della funzione indica
     //measurement = 512;
     char fakeChar = 0;
     int sC = 0;
     for(sC = 1;sC<=4;sC++)
     {
        fakeChar = measurement>>8*(4-sC);
        txData[sC-1]=fakeChar;
     }
}
void activateLed(char device)
{
    switch(device)
    {
        case '0':
        {
            LATDbits.LATD5 = 1;
            break;
        }
        case '1':
        {
            LATDbits.LATD6 = 1;
            break;
        }
        case '2':
        {
            LATDbits.LATD7 = 1;
            break;
        }
        case '4':
        {
            buzzerFlag = 1;
            break;
        }
    }
}
void turnOffLed(char device)
{
    switch(device)
    {
        case '0':
        {
            LATDbits.LATD5 = 0;
            break;
        }
        case '1':
        {
            LATDbits.LATD6 = 0;
            break;
        }
        case '2':
        {
            LATDbits.LATD7 = 0;
            break;
        }
        case '4':
        {
            buzzerFlag = 0;
            LATDbits.LATD5 = 0;
            break;
        }
    }
}
char getLedStatus(char device)
{
    char data;
    switch(device)
    {
        case '0':
        {
            data = LATDbits.LATD5;
            break;
        }
        case '1':
        {
            data = LATDbits.LATD6;
            break;
        }
        case '2':
        {
            data = LATDbits.LATD7;
            break;
        }
    }
    return data;
}

int main() 
{
    TRISBbits.TRISB14 = 0;
    LATBbits.LATB14 = 1;
    TRISDbits.TRISD8 = 0;
    LATDbits.LATD8 = 0;
    TRISDbits.TRISD5 = 0;
    LATDbits.LATD5 = 0;
    TRISDbits.TRISD6 = 0;
    LATDbits.LATD6 = 0;
    TRISDbits.TRISD7 = 0;
    LATDbits.LATD7 = 0;
    TRISGbits.TRISG6 = 1;
    send = 0;
    initializeADC(); // Initialize the ADC10
    // nel main
    /*OpenUART1(UART_EN, UART_RX_ENABLE | UART_TX_ENABLE, BAUD_VALUE);

    // Configure UART1 RX Interrupt
    ConfigIntUART1(UART_INT_PR2 | UART_RX_INT_EN | UART_TX_INT_EN);
*/
    unsigned int pbClk=SYSTEMConfig( SYSCLK, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
 // Abilita UART1 and set baud rate to DESIRED_BAUDRATE=9600
    OpenUART1( UART_EN, UART_RX_ENABLE | UART_TX_ENABLE, pbClk/16/BAUD_VALUE-1);
    ConfigIntUART1(UART_INT_PR1 | UART_RX_INT_EN | UART_TX_INT_EN);
    while( BusyUART1());
    OpenTimer2( T2_ON | T2_SOURCE_INT | T2_PS_1_64, 0x2FFF);
    ConfigIntTimer2( T2_INT_ON | T2_INT_PRIOR_2);
    // Must enable glocal interrupts - in this case, we are using multi-vector mode
    INTEnableSystemMultiVectoredInt();

    while (1) // Loop forever
    {   
        if (addressRx == ADDRESS && i == 0) 
        {
            switch(cmd)
            {
                case 'A':
                {
                    activateLed(device);
                    break;
                }
                case 'B':
                {
                    turnOffLed(device);
                    break;
                }
                case'C':
                {
                    switch(devices[dvCounter])
                    {
                        case '0':case'1':case'2':case '4':
                        {
                            txData[0]=0x00;
                            txData[1]=0x00;
                            txData[2]=0x00;
                            txData[3] = getLedStatus(devices[dvCounter]);
                            break;
                        }
                        default:
                        {
                            getAnalogMeasurement(devices[dvCounter]);
                            break;
                        }
                    }
                    send = 1;
                    break;
                }
            }
            addressRx = 0;
            cmd = 0;
        }
        if(send)
        {
            sendData();
        }
        

    }
    return 1;
}

void __ISR(_UART1_VECTOR, ipl2) IntUart1Handler(void) 
{
    // Is this an RX interrupt?
    if (mU1RXGetIntFlag()) 
    {
        rxDATA = (char)ReadUART1();
        if(rxDATA != 0xa)
        {
            switch(i)
            {
                case 0:{
                    addressRx = rxDATA;
                    i++;
                    break;
                }
                case 1:{
                    addressTx = rxDATA;
                    i++;
                    break;
                }
                case 2:{
                    cmd = rxDATA;
                    i++;
                    break;
                }
                case 3:{
                    device = rxDATA;
                    i++;
                    break;
                }
                default:{
                    data[counter] = rxDATA;
                    if(i == 7){
                        i=0;
                        counter = 0;
                    }
                    else {
                        counter++;
                        i++;
                    }
                    
                }
            }
        }
        mU1RXClearIntFlag();
    }

    // We don't care about TX interrupt
    if (mU1TXGetIntFlag()) {
        mU1TXClearIntFlag();
    }
}
void __ISR(_TIMER_2_VECTOR, ipl2) handlesTimer2Ints(void){
 // **make sure iplx matches the timer?s interrupt priority level
    if(buzzerFlag)
  LATDbits.LATD5 = ~LATDbits.LATD5;

  mT2ClearIntFlag();
 // Clears the interrupt flag so that the program returns to the main loop
} // END Timer2 ISR